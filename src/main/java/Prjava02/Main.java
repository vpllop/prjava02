/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Prjava02;

import java.io.IOException;
import java.net.InetAddress;

/**
 *
 * @author vpllo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * @param args the command line arguments
         */
        System.out.println("versi� 0.1 del projecte prjava02");
        System.out.println("Creacio? d'una branca del projecte prjava02");
        System.out.println("Afegint me?s codi a la branca00 del projecte prjava02");
        try {
            InetAddress adre�a = InetAddress.getLocalHost();
            String hostname = adre�a.getHostName();
            System.out.println("hostname=" + hostname);
            System.out.println("Nom de l'usuari: " + System.getProperty("user.name"));
            System.out.println("Carpeta Personal: " + System.getProperty("user.home"));
            System.out.println("Sistema operatiu: " + System.getProperty("os.name"));
            System.out.println("Versi� OS: " + System.getProperty("os.version"));
        } catch (IOException e) {
            System.out.println("Exception occurred");
        }
    }
}
